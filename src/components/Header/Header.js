import React from 'react'
import { Link, useNavigate } from 'react-router-dom';
import './Header.scss';

 function Header() {
  const navigate = useNavigate();

  const logout = () => {
    localStorage.removeItem('isLoggedIn');
    navigate("/login", { replace: true });
  }

  return (
    <div className='main-header'>
        <div className='main-header-container'>
            <Link to='/'>First-project</Link>
            <div>
              <Link to='/about'>About us</Link>
              <button onClick={logout}
                  className='main-header-container-btn'>
                 Log out
              </button>
            </div>
        </div>
    </div>
  )
}
export default Header;