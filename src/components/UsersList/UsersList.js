import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import { data } from '../../mock/dummyData';
import './UsersList.scss';

 function UsersList() {
    const [users, setUsers] = useState(data);

    const removeUser = (userToRemove) => {
        setUsers(users.filter(user => user != userToRemove));
    }

  return (
    <div className='users-list'>
        {users.map(user => (
            <Link to={'user/' + user.id} >
                <div className='user-container'>
                    <p>{user.name}</p>
                    <span className='user-container-delete' onClick={(e) => {
                        e.stopPropagation();
                        removeUser(user);
                    }}>
                        <svg fill="red" 
                            xmlns="http://www.w3.org/2000/svg" 
                            viewBox="0 0 30 30" 
                            width="30px" height="30px">
                                <path d="M20.222,26H9.778c-1.014,0-1.868-0.759-1.986-1.766L6,9h18l-1.792,15.234C22.089,25.241,21.236,26,20.222,26z"/>
                                <path fill="none" stroke="red" strokeLinecap="round" strokeMiterlimit="10" strokeWidth="2" d="M8.5 5L21.5 5M15 3.5L15 5.5M6 6L24 6"/>
                        </svg>
                    </span>
                </div>
            </Link>
        ))}
    </div>
  )
}

export default UsersList;