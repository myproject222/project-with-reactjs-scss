import React, { useState, useEffect } from 'react';
import {useParams} from 'react-router-dom';
import { data } from '../../mock/dummyData';

 function SingleUserPage(props) {
    const [userId, setUserId] = useState(null);
    const [user, setUser] = useState({});

    const { id } = useParams();

    useEffect(() => {
        setUserId(id);

        setUser(data.find(user => user?.id == id));
    }, [user])

  return (
    <div>
        <span className='user-key'>Name: </span>
        <span className='user-value'>{user?.name}</span> <br />
        <span className='user-key'>Email: </span>
        <span className='user-value'>{user?.email}</span> <br />
        <span className='user-key'>Address: </span>
        <span className='user-value'>{user?.address?.suite + ' ' + user?.address?.street}</span>
    </div>
  )
}

export default SingleUserPage;