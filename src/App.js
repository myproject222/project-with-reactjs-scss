import { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route, useNavigate, Navigate } from 'react-router-dom';
import './App.scss';
import Header from './components/Header/Header';
import AboutPage from './pages/AboutPage/AboutPage';
import Homepage from './pages/Homepage/Homepage';
import LoginPage from './pages/LoginPage/LoginPage';
import SingleUserPage from './pages/SingleUserPage/SingleUserPage';

function App() {
  const [showHeader, setShowHeader] = useState(false);

  const checkIfUserLoggedIn = () => {
    // Metoda per konvertimin e cdo vlere ne true nese ekziston dhe 
    // eshte jo falsey value por nese eshte falsey value beje false
    setShowHeader(!!localStorage.getItem('isLoggedIn'));
    return localStorage.getItem('isLoggedIn');
  }
  
  const PublicRoute = ({ children }) => {
    return !checkIfUserLoggedIn() ? children : <Navigate to="/" />;
  }

  const PrivateRoute = ({ children }) => {
    return checkIfUserLoggedIn() ? children : <Navigate to="/login" />;
  }

  return (
    <div className="App">
      <BrowserRouter>
      {showHeader && <Header />}
        <Routes>
        <Route
              path="/login"
              element={
                <PublicRoute>
                  <LoginPage />
                </PublicRoute>
              }
            />
            <Route
              path="/"
              element={
                <PrivateRoute>
                  <Homepage />
                </PrivateRoute>
              }
            />
            <Route
              path="/about"
              element={
                <PrivateRoute>
                  <AboutPage />
                </PrivateRoute>
              }
            />
            <Route
              path="/user/:id"
              element={
                <PrivateRoute>
                  <SingleUserPage />
                </PrivateRoute>
              }
            />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
