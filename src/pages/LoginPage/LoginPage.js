import React from 'react';
import { useNavigate } from 'react-router-dom';
import './LoginPage.scss';

function LoginPage() {
    const navigate = useNavigate();
    
    const login = () => {
        localStorage.setItem('isLoggedIn', 'true');
        navigate("../", { replace: true });
    }


  return (
    <div className='login'>
        <div className='login-container'>
            <h2>You are not logged in</h2>
            <div className='login-container-btn'>
                <button onClick={login}>Login here</button>
            </div>
        </div>
    </div>
  )
}

export default LoginPage;