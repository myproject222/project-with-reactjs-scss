import React from 'react'
import UsersList from '../../components/UsersList/UsersList';

function Homepage() {
  return (
    <div>
        <UsersList />
    </div>
  )
}

export default Homepage;